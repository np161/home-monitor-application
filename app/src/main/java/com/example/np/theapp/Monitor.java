package com.example.np.theapp;

import android.app.Activity;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

public class Monitor extends AsyncTask<Void,Void,Void> {

    boolean monitoring = false;
    Context context;
    String latitude;
    String longitude;
    int monitortime=30000;
    int monitorInterval=10000;
    double initialLat;
    double initialLong;
    GPSReader gps;
    //Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    //final Ringtone r = RingtoneManager.getRingtone(context, notification);

    private Exception exception;

    protected Void doInBackground(Void... params) {
        if (monitoring == false){
            monitoring = true;
            int totalTime = 0;
            //updateText("Currently Monitoring");

            while (monitoring == true) {
                if (gps.canGetLocation()) {
                    initialLat = gps.getLatitude();
                    initialLong = gps.getLongitude();
                } else {
                    gps.showSettingsAlert();
                }
                try {
                    Thread.sleep(monitorInterval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                double tempLat = gps.getLatitude();
                double tempLong = gps.getLongitude();
                boolean result = compareLocation(initialLat,initialLong,tempLat,tempLong);
                if (result == false){
                    new SendAttachment(MyActivity.user.getToNotify(), MyActivity.user.getEmail(), MyActivity.user.getMailPassword(),"User " + MyActivity.user.getUsername() + " has not moved from latitude for "+monitortime+" and may need assitance at " + MyActivity.user.getAddress() ).execute();
                }
                totalTime += monitorInterval;
                if (totalTime == monitortime) {
                    monitoring = false;
                    //Toast.makeText(getApplicationContext(), "Monitoring Over", Toast.LENGTH_SHORT).show();
                    //updateText("Not Monitoring");
                }
            }
        } else if (monitoring == true) {
            //Toast.makeText(getApplicationContext(), "currently monitoring", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    public Monitor( Context actContext, GPSReader gps ){
        context = actContext;
        this.gps = gps;
    }

    public boolean compareLocation( double initialLat, double initialLong, double newLat, double newLong ){
        boolean safe = true;
        boolean latSafe = true;
        boolean longSafe = true;
        if( (newLat - initialLat) > 0.0001 ||(newLat - initialLat) <= -0.0001){
            latSafe = false;
        }
        if( (newLong - initialLong) > 0.0001 ||(newLong - initialLong) <= -0.0001){
            longSafe = false;
        }
        if( latSafe == false || longSafe == false ){
            safe = false;
        }

        return safe;
    }
}
