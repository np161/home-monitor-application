package com.example.np.theapp;


public class UserDetails {
    String username;
    String password;
    String rePassword;
    String address;
    String email;
    String toNotify;
    String mailPassword;
    String userToNotify;
    int moniterTime;
    int moniterInterval;
    int maxHeartrate;
    int minHeartrate;

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }

    public String getAddress(){
        return address;
    }

    public String getEmail(){
        return email;
    }

    public String getToNotify(){
        return toNotify;
    }

    public String getMailPassword(){
        return mailPassword;
    }

    public String getUserToNotify(){ return userToNotify; }

    public void setToNotify( String mail ){
        this.toNotify = mail;
    }

    public void setMoniterTime( int time ){
        this.moniterTime = time;
    }

    public void setMoniterInterval( int interval ){
        this.moniterInterval=interval;
    }

    public void setMaxHeartrate( int heartrate ){
        this.maxHeartrate = heartrate;
    }

    public void setMinHeartrate( int heartrate ){
        this.minHeartrate = heartrate;
    }

    public void setUserToNotify( String name ){ this.userToNotify = name; }

    public int getMonitorTime(){
        return moniterTime;
    }

    public int getMoniterInterval(){
        return moniterInterval;
    }

    public int getMaxHeartrate(){
        return maxHeartrate;
    }

    public int getMinHeartrate(){
        return minHeartrate;
    }

    public void saveInDB(){
        LoginDataBaseAdapter.updateEntry(username,password,rePassword,address,toNotify,email,mailPassword,moniterTime,moniterInterval,maxHeartrate,minHeartrate,userToNotify);
    }
}