package com.example.np.theapp;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.parse.Parse;
import com.parse.ParseInstallation;


public class Registration extends Activity{

    LoginDataBaseAdapter loginDataBaseAdapter;
    EditText password;
    EditText repassword;
    EditText securityhint;
    EditText username;
    EditText address;
    EditText email;
    EditText notifyEmail;
    EditText mailPassword;
    Button register;
    Button cancel;
    CheckBox check;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        loginDataBaseAdapter = new LoginDataBaseAdapter(this);
        loginDataBaseAdapter=loginDataBaseAdapter.open();
        password=(EditText)findViewById(R.id.password_edt);
        repassword=(EditText)findViewById(R.id.repassword_edt);
        securityhint=(EditText)findViewById(R.id.securityhint_edt);
        register=(Button)findViewById(R.id.register_btn);
        cancel=(Button)findViewById(R.id.cancel_btn);
        check=(CheckBox)findViewById(R.id.checkBox1);
        username=(EditText)findViewById(R.id.username_edt);
        address=(EditText)findViewById(R.id.address_edt);
        email=(EditText)findViewById(R.id.email_edt);
        notifyEmail=(EditText)findViewById(R.id.notifyEmail_edt);
        mailPassword=(EditText)findViewById(R.id.mailPassword_edt);
        Parse.initialize(this, "o7BGRO0lwvZlyawOIWRZrH7ceTfWh33FhBRkxZwk", "fsIqybbsiUsdL0OexK1U6ZBuYsxLewpgTxTOGrHP");
        ParseInstallation.getCurrentInstallation().saveInBackground();


        check.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                else{
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });


        register.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String user=username.getText().toString();
                String Pass=password.getText().toString();
                String Secu=securityhint.getText().toString();
                String Repass=repassword.getText().toString();
                String ema=email.getText().toString();
                String add=address.getText().toString();
                String notEma=notifyEmail.getText().toString();
                String emaPW=mailPassword.getText().toString();


                if(Pass.equals("")||Repass.equals("")||Secu.equals("")){
                    Toast.makeText(getApplicationContext(), "Fill All Fields", Toast.LENGTH_LONG).show();
                    return;
                }

                if(!Pass.equals(Repass)){
                    Toast.makeText(getApplicationContext(), "Password does not match", Toast.LENGTH_LONG).show();
                    return;
                }
                else{
                    loginDataBaseAdapter.insertEntry(user, Pass,Repass,Secu,add,notEma,ema,emaPW,"");

                    Toast.makeText(getApplicationContext(), "Account Successfully Created ", Toast.LENGTH_LONG).show();
                    Log.d("PASSWORD",Pass);
                    Log.d("RE PASSWORD",Repass);
                    Log.d("SECURITY HINT",Secu);
                    Log.d("USERNAME", user);
                    Log.d("ADDRESS", add);
                    Log.d("EMAIL", ema);
                    Log.d("NOTIFYEMAIL",notEma);
                    Log.d("MAILPASSWORD",emaPW);
                    Intent i=new Intent(Registration.this,MyActivity.class);
                    startActivity(i);
                }
            }
        });
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii=new Intent(Registration.this,MyActivity.class);
                startActivity(ii);
            }
        });

    }

}