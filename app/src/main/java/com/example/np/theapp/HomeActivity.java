package com.example.np.theapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParsePush;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends Activity{
    Button sender;
    Button monitorStrt;
    Button monitorEnd;
    Button settings;
    Button heartrate;
    static TextView homeMessage;
    boolean monitoring = false;
    GPSReader gps;
    static int monitortime=20000;
    static int monitorInterval=10000;
    double initialLat;
    double initialLong;
    int totalTime = 0;
    double tempLat;
    double tempLong;
    boolean result;
    int gpscount=0;
    static final int MSG_DISMISS_DIALOG = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        sender = (Button)findViewById(R.id.logout);
        monitorStrt = (Button)findViewById(R.id.moniter_strt);
        monitorEnd = (Button)findViewById(R.id.moniter_end);
        settings = (Button)findViewById(R.id.settings);
        homeMessage = (TextView)findViewById(R.id.textView1);
        heartrate = (Button)findViewById(R.id.heartrate);

        Parse.initialize(this, "o7BGRO0lwvZlyawOIWRZrH7ceTfWh33FhBRkxZwk", "fsIqybbsiUsdL0OexK1U6ZBuYsxLewpgTxTOGrHP");
        ParseInstallation.getCurrentInstallation().saveInBackground();


        monitorStrt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runnable runnable = new Runnable(){
                    @Override
                    public void run() {
                        Looper.prepare();
                        if(monitoring==false){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast toast = Toast.makeText(getApplicationContext(), "Monitoring Started", Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                            });
                            monitoring = true;
                            while (monitoring == true) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        HomeActivity.homeMessage.setText("Monitoring");
                                    }
                                });

                                gps = new GPSReader(getApplicationContext());
                                if (gps.canGetLocation()) {
                                    initialLat = gps.getLatitude();
                                    initialLong = gps.getLongitude();
                                } else {
                                    gps.showSettingsAlert();
                                }
                                ParsePush push = new ParsePush();
                                push.setChannel(MyActivity.user.getUserToNotify());
                                push.setMessage("User " + MyActivity.user.getUsername() + " has not moved from their location for " + monitortime + " and may need assitance at " + MyActivity.user.getAddress());
                                push.sendInBackground();
                                try {
                                    Thread.sleep(monitorInterval);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                if (tempLat != 0d && tempLong != 0d) {
                                   result = compareLocation(initialLat, initialLong, tempLat, tempLong);
                                }
                                tempLat = gps.getLatitude();
                                tempLong = gps.getLongitude();

                                if (result == false) {
                                    gpscount++;
                                }
                                totalTime += monitorInterval;
                                if (totalTime == monitortime && gpscount != 0) {
                                    Runnable runnable = new Runnable(){
                                        @Override
                                        public void run() {
                                            runOnUiThread( new Runnable() {
                                                @Override
                                                public void run() {
                                                    final Timer t = new Timer();
                                                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            switch (which){
                                                                case DialogInterface.BUTTON_POSITIVE:
                                                                    new SendAttachment(MyActivity.user.getToNotify(), MyActivity.user.getEmail(), MyActivity.user.getMailPassword(), "User " + MyActivity.user.getUsername() + " has not moved from their location for " + monitortime + " and may need assitance at " + MyActivity.user.getAddress() ).execute();
                                                                    homeMessage.setText("Not Monitoring");
                                                                    t.cancel();
                                                                    monitoring = false;
                                                                    break;

                                                                case DialogInterface.BUTTON_NEGATIVE:
                                                                    homeMessage.setText("Not Monitoring");
                                                                    t.cancel();
                                                                    monitoring = false;
                                                                    break;

                                                            }
                                                        }
                                                    };


                                                    AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                                                    builder.setMessage("Do you require assistance?").setPositiveButton("Yes", dialogClickListener)
                                                            .setNegativeButton("No", dialogClickListener).show();
                                                    builder.setCancelable(true);
                                                    final AlertDialog alert = builder.create();

                                                    t.schedule(new TimerTask() {
                                                        public void run() {
                                                            new SendAttachment(MyActivity.user.getToNotify(), MyActivity.user.getEmail(), MyActivity.user.getMailPassword(), "User " + MyActivity.user.getUsername() + " has not moved from their location for " + monitortime + " and may need assitance at " + MyActivity.user.getAddress() ).execute();
                                                            alert.dismiss(); // when the task active then close the dialog
                                                            t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
                                                        }
                                                    }, 4000);



                                                }
                                            });
                                        }
                                    };
                                    Thread myThread = new Thread(runnable);
                                    myThread.start();
                                }
                            }
                        }else if(monitoring == true){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(),"Already Monitoring",Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                };
                Thread myThread = new Thread(runnable);
                myThread.start();
            }
        });

        monitorEnd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Runnable runnable = new Runnable(){
                    @Override
                    public void run() {
                        runOnUiThread( new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(HomeActivity.this,"set to false",Toast.LENGTH_SHORT).show();
                                homeMessage.setText("Not Monitoring");
                                monitoring = false;
                            }
                        });
                        monitoring = false;
                    }
                };
            Thread myThread = new Thread(runnable);
                myThread.start();
            }
        });

        settings.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent ii = new Intent(HomeActivity.this, Settings.class);
                startActivity(ii);
            }
        });

        sender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(HomeActivity.this, MyActivity.class);
                startActivity(ii);
                MyActivity.user = new UserDetails();
            }
        });

        heartrate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent ii = new Intent(HomeActivity.this, HeartRateMonitor.class);
                startActivity(ii);
            }
        });
    }

    public void updateText( String message ){
        homeMessage.setText(message);
    }

    public boolean compareLocation( double initialLat, double initialLong, double newLat, double newLong ){
        boolean safe = true;
        boolean latSafe = true;
        boolean longSafe = true;
        if( (newLat - initialLat) > 0.0002 ||(newLat - initialLat) <= -0.0002){
            latSafe = false;
        }
        if( (newLong - initialLong) > 0.0002 ||(newLong - initialLong) <= -0.0002){
            longSafe = false;
        }
        if( latSafe == false || longSafe == false ){
            safe = false;
        }

        return safe;
    }
}
