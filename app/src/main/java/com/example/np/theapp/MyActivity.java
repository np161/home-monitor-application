package com.example.np.theapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Dialog;
import android.content.Intent;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParsePush;

public class MyActivity extends Activity {

    LoginDataBaseAdapter loginDataBaseAdapter;
    Button login;
    Button registerr;
    EditText username;
    EditText enterpassword;
    EditText address;
    EditText email;
    Button forgetpass;
    String userName;
    static UserDetails user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        login = (Button) findViewById(R.id.login_btn);
        registerr = (Button) findViewById(R.id.register_btn);
        username = (EditText) findViewById(R.id.username_edt);
        enterpassword = (EditText) findViewById(R.id.password_edt);
        address = (EditText) findViewById(R.id.address_edt);
        email = (EditText) findViewById(R.id.email_edt);
        forgetpass = (Button) findViewById(R.id.textView2);
        Parse.initialize(this, "o7BGRO0lwvZlyawOIWRZrH7ceTfWh33FhBRkxZwk", "fsIqybbsiUsdL0OexK1U6ZBuYsxLewpgTxTOGrHP");
        ParseInstallation.getCurrentInstallation().saveInBackground();


        loginDataBaseAdapter = new LoginDataBaseAdapter(getApplicationContext());
        loginDataBaseAdapter.open();

        registerr.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyActivity.this, Registration.class);
                startActivity(i);
            }
        });

        login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                userName = username.getText().toString();
                String passWord = enterpassword.getText().toString();
                if (userName.equals("")) {
                    Toast.makeText(MyActivity.this, "Please Enter Your Username", Toast.LENGTH_SHORT).show();
                } else {

                    user = loginDataBaseAdapter.getUser(userName);

                    String storedPassword = user.getPassword();

                    if (passWord.equals(storedPassword)) {
                        Toast.makeText(MyActivity.this, "Congrats: Login Successfully", Toast.LENGTH_SHORT);
                        ParsePush.subscribeInBackground(MyActivity.user.getUserToNotify());
                        Intent ii = new Intent(MyActivity.this, HomeActivity.class);
                        startActivity(ii);
                    } else {
                        Toast.makeText(MyActivity.this, "Password Incorrect", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        forgetpass.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(MyActivity.this);
                dialog.getWindow();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.forget_search);
                dialog.show();

                final EditText user = (EditText) dialog.findViewById(R.id.username_edt);
                final EditText security = (EditText) dialog.findViewById(R.id.securityhint_edt);
                final TextView getpass = (TextView) dialog.findViewById(R.id.textView5);

                Button ok = (Button) dialog.findViewById(R.id.getpassword_btn);
                Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);

                ok.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {

                        String userName = user.getText().toString();
                        String hint = security.getText().toString();

                        if (userName.equals("")) {
                            Toast.makeText(getApplicationContext(), "Please enter your username", Toast.LENGTH_SHORT).show();
                        }else if( hint.equals("")){
                            Toast.makeText(getApplicationContext(), "Please enter your security hint", Toast.LENGTH_SHORT).show();
                        } else {
                            String storedPassword = loginDataBaseAdapter.getAllTags(userName);
                            if (storedPassword == null) {
                                Toast.makeText(getApplicationContext(), "Please enter correct security hint", Toast.LENGTH_SHORT).show();
                            } else {
                                Log.d("GET PASSWORD", storedPassword);
                                getpass.setText(storedPassword);
                            }
                        }
                    }
                });
                cancel.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}


