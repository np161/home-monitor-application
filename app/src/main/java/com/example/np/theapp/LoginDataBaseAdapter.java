package com.example.np.theapp;


import java.util.HashMap;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;


public class LoginDataBaseAdapter {

    static final String DATABASE_NAME = "users.db";
    static final int DATABASE_VERSION = 1;
    public static final int NAME_COLUMN = 13;

    static final String DATABASE_CREATE = "create table "+"USERS"+"( " +"ID integer primary key autoincrement,"+"USERNAME  text,PASSWORD  text,REPASSWORD text,SECURITYHINT text,ADDRESS text,NOTIFYEMAIL text,EMAIL text, EMAILPASSWORD text, MONITERTIME int, MONITERINTERVAL int, MAXHEARTRATE int, MINHEARTRATE int, USERTONOTIFY text )";

    public static final String[] ALL_KEYS = new String[] {"USERNAME","PASSWORD","REPASSWORD","SECURITYHINT","ADDRESS","NOTIFYEMAIL","EMAIL","EMAILPASSWORD", "MONITERTIME","MONITERINTERVAL","MAXHEARTRATE","MINHEARTRATE","USERTONOTIFY"};

    public static SQLiteDatabase db;
    private final Context context;
    private DataBaseHelper dbHelper;

    public  LoginDataBaseAdapter(Context _context){
        context = _context;
        dbHelper = new DataBaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public  LoginDataBaseAdapter open() throws SQLException{
        db = dbHelper.getWritableDatabase();
        return this;
    }
    public void close(){
        db.close();
    }

    public  SQLiteDatabase getDatabaseInstance(){
        return db;
    }

    public void insertEntry(String username,String password,String repassword,String securityhint,String address,String toNotify, String email, String emailPassword, String name){
        ContentValues newValues = new ContentValues();
        newValues.put("USERNAME",username);
        newValues.put("PASSWORD", password);
        newValues.put("REPASSWORD",repassword);
        newValues.put("SECURITYHINT",securityhint);
        newValues.put("ADDRESS",address);
        newValues.put("NOTIFYEMAIL", toNotify);
        newValues.put("EMAIL",email);
        newValues.put("EMAILPASSWORD", emailPassword);
        newValues.put("MONITERTIME", 7200000);
        newValues.put("MONITERINTERVAL",1800000);
        newValues.put("MAXHEARTRATE",150);
        newValues.put("MINHEARTRATE",50);
        newValues.put("USERTONOTIFY", "");

        db.insert("USERS", null, newValues);
    }

    public Cursor getAllRows(){
        String where = null;
        Cursor c = 	db.query(true, "USERS", ALL_KEYS, where, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    public boolean deleteRow(long rowId) {
        String where = "ID" + "=" + rowId;
        return db.delete("USERS", where, null) != 0;
    }

    public UserDetails getUser( String username ){
        UserDetails user = new UserDetails();
        Cursor cursor=db.query("USERS", null, " USERNAME=?", new String[]{username}, null, null, null);
        if(cursor.getCount()<1) // UserName Not Exist
        {
            cursor.close();
            Toast.makeText(context, "no username",Toast.LENGTH_LONG).show();
        }
        cursor.moveToFirst();
        user.username = username;
        user.password = cursor.getString(cursor.getColumnIndex("PASSWORD"));
        user.rePassword = cursor.getString(cursor.getColumnIndex("REPASSWORD"));
        user.email = cursor.getString(cursor.getColumnIndex("EMAIL"));
        user.address = cursor.getString(cursor.getColumnIndex("ADDRESS"));
        user.toNotify = cursor.getString(cursor.getColumnIndex("NOTIFYEMAIL"));
        user.mailPassword = cursor.getString(cursor.getColumnIndex("EMAILPASSWORD"));
        user.moniterTime = cursor.getInt(cursor.getColumnIndex("MONITERTIME"));
        user.moniterInterval = cursor.getInt(cursor.getColumnIndex("MONITERINTERVAL"));
        user.maxHeartrate = cursor.getInt(cursor.getColumnIndex("MAXHEARTRATE"));
        user.minHeartrate = cursor.getInt(cursor.getColumnIndex("MINHEARTRATE"));
        user.userToNotify = cursor.getString(cursor.getColumnIndex("USERTONOTIFY"));

        cursor.close();
        return user;
    }

    public String getAllTags(String a) {
        Cursor c = db.rawQuery("SELECT * FROM " + "USERS" + " where SECURITYHINT = '" +a + "'" , null);
        String str = null;
        if (c.moveToFirst()) {
            do {
                str = c.getString(c.getColumnIndex("PASSWORD"));
            } while (c.moveToNext());
        }
        return str;
    }

    public static void updateEntry(String username,String password,String repassword,String address,String toNotify, String email, String emailPassword,int moniterTime,int moniterInterval,int maxHeartrate,int minHeartrate, String name){
        ContentValues updatedValues = new ContentValues();
        updatedValues.put("USERNAME",username);
        updatedValues.put("PASSWORD", password);
        updatedValues.put("REPASSWORD",repassword);
        updatedValues.put("SECURITYHINT",repassword);
        updatedValues.put("ADDRESS", address);
        updatedValues.put("NOTIFYEMAIL", toNotify);
        updatedValues.put("EMAIL",email);
        updatedValues.put("EMAILPASSWORD", emailPassword);
        updatedValues.put("MONITERTIME",moniterTime);
        updatedValues.put("MONITERINTERVAL",moniterInterval);
        updatedValues.put("MAXHEARTRATE",maxHeartrate);
        updatedValues.put("MINHEARTRATE",minHeartrate);
        updatedValues.put("USERTONOTIFY", name);

        String where="USERNAME = ?";
        db.update("USERS",updatedValues, where, new String[]{username});
    }

}