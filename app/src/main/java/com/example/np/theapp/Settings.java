package com.example.np.theapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParsePush;


public class Settings extends Activity {

    EditText monitorTime;
    EditText monitorInterval;
    EditText notifyMail;
    EditText maxHeartrate;
    EditText minHeartrate;
    EditText userNotify;
    Button save;
    Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        Parse.initialize(this, "o7BGRO0lwvZlyawOIWRZrH7ceTfWh33FhBRkxZwk", "fsIqybbsiUsdL0OexK1U6ZBuYsxLewpgTxTOGrHP");
        ParseInstallation.getCurrentInstallation().saveInBackground();

        monitorTime=(EditText)findViewById(R.id.monitorTime_edt);
        monitorInterval=(EditText)findViewById(R.id.monitorInterval_edt);
        notifyMail=(EditText)findViewById(R.id.notifyEmail_edt);
        userNotify=(EditText)findViewById(R.id.userToNotify_edt);
        maxHeartrate=(EditText)findViewById(R.id.heartrateMax_edt);
        minHeartrate=(EditText)findViewById(R.id.heartrateMin_edt);

        save=(Button)findViewById(R.id.save);
        cancel=(Button)findViewById(R.id.cancel_btn);

        monitorTime.setText(String.valueOf(convertToHours(MyActivity.user.getMonitorTime())));
        monitorInterval.setText(String.valueOf(convertToMins(MyActivity.user.getMoniterInterval())));
        maxHeartrate.setText(String.valueOf(MyActivity.user.getMaxHeartrate()));
        minHeartrate.setText(String.valueOf(MyActivity.user.getMinHeartrate()));
        notifyMail.setText(MyActivity.user.getToNotify());
        if(MyActivity.user.getUserToNotify().equals("")){
            userNotify.setText("(Not Set)");
        }else{
            userNotify.setText(MyActivity.user.getUserToNotify());
        }



        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                saveChanges();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Changes Saved", Toast.LENGTH_SHORT).show();
                    }
                });
                finish();
            }
        });
    }

    public int convertToHours(int value){
        int result;
        result = value/3600000;
        return result;
    }

    public int convertFromHours(int value){
        int result;
        result = value*3600000;
        return result;
    }

    public int convertToMins(int value){
        int result;
        result = value/60000;
        return result;
    }

    public int convertFromMins(int value){
        int result;
        result=value*60000;
        return result;
    }

    public void saveChanges(){
        int monitorTimeChange = Integer.parseInt(monitorTime.getText().toString());
        int monitorIntervalChange = Integer.parseInt(monitorInterval.getText().toString());
        int heartrateMax = Integer.parseInt(maxHeartrate.getText().toString());
        int heartrateMin = Integer.parseInt(minHeartrate.getText().toString());
        String notifyMailChange = notifyMail.getText().toString();
        String userNotifyChange = userNotify.getText().toString();
        ParsePush.subscribeInBackground(userNotifyChange);

        monitorTimeChange = convertFromHours(monitorTimeChange);
        monitorIntervalChange = convertFromMins(monitorIntervalChange);

        MyActivity.user.setMoniterTime(monitorTimeChange);
        MyActivity.user.setMoniterInterval(monitorIntervalChange);
        MyActivity.user.setToNotify(notifyMailChange);
        MyActivity.user.setMaxHeartrate(heartrateMax);
        MyActivity.user.setMinHeartrate(heartrateMin);
        MyActivity.user.setUserToNotify(userNotifyChange);

        MyActivity.user.saveInDB();
    }
}
